# RSA encrypt and decrypt



## Getting started

To make it easy for you to get started with RSA Encrypt and RSA Decrypt. It is an example about RSA Encrypt and RSA Decrypt  

## How to generate public key and private keyby OpenSSL

- Step 1: Generate a 2048-bit RSA private key
```
$ openssl genrsa -out private.pem 2048
```
- Step 2: Convert private Key to PKCS#8 format (so Java can read it)
```
$ openssl pkcs8 -topk8 -inform PEM -outform PEM -in private.pem -out private_key. pem -nocrypt
```
- Step 3: Output public key portion in DER format (so Java can read it)
 ```
$ openssl rsa -in private_key.pem -pubout -outform PEM -out public_key.pem
```
## RSA Encrypt
- API URL: http://localhost:8080/encrypt
- Method: POST
- Request Body:
```
{
    "fullname": "Nguyen Van A",
    "dob": "",
    "nid1":"123456789",
    "nid2":""
}
```
- Response:
```
{
    "data": "jxCxbZBUnxb+w1KTbhsqEZI7b4NPFn9ZpaqLRiRI8GUJB4QJROxZ8YgTwTYyF3zLzou22ByQrftiGRWg8QcBXoGOpV8nRw==",
    "sign": "CAL6jPkOUgNk6yMcDpkbTP725HxrQMAv+MHe5pjOp8LiSPkWe3QDq1vNqi+fWl4m0N3NmwIS5djEi9hnUc0MJfP08Buz6lEyEoLJlttb7AV969mChjGmiK0+KGZg4xg6ITsgUMn8aVNQJ7CtVF3SO3miELtJFbBAoMLFLhAcKI6VfmFkytlQywFj2HMHhKcoJIL+NWYNLZUYxea2kQ+fpIEuvYRTE0XqTdQNvDqXcVpsopmCcmZmvETVtXnwRhpS9ok8j+LcntQH/a+p25SWVIlLoQyCV+dJ9CnFh6bAj7gWSYOgDlWY1u2iAvYm+NGZH0nZHkPEXYWXQoyDZvl30w==",
    "key": "XgpUzAkgk4DjLTAxqSPZ7R2mf6NFkdFoFQF1vf+bEsq0CFmAprEtPpwdhKxqi2VDAnIW2xq+411b2LHYrKkPPQ5VAESOPTDGhlMnT/SI6T6IULDwaSXUHw+u9tvfnwzfHRqM5PinQmCrV+NMPRumHKVPNGPcL+bh+5YRWm0E2ZzoPctOKlpXu8B5/8wYQeSFEbIWATL1tazmmcvZrzXKus5OhWU5Orfi6njEM2mw7V4ymlhVdLpxwSbRFQf3WAjqFvrqJl4Gjfn6Ki3ir0Otl8EOe1bQanKX0v+eAOgNlxfE++dXx/x0szgIy/q0yo/dRHbxWaTQAo4PIuC8wDAokQ=="
}
```

## RSA Decrypt
- API URL: http://localhost:8080/decrypt
- Method: POST
- Request Body:
```
{
    "data": "jxCxbZBUnxb+w1KTbhsqEZI7b4NPFn9ZpaqLRiRI8GUJB4QJROxZ8YgTwTYyF3zLzou22ByQrftiGRWg8QcBXoGOpV8nRw==",
    "sign": "CAL6jPkOUgNk6yMcDpkbTP725HxrQMAv+MHe5pjOp8LiSPkWe3QDq1vNqi+fWl4m0N3NmwIS5djEi9hnUc0MJfP08Buz6lEyEoLJlttb7AV969mChjGmiK0+KGZg4xg6ITsgUMn8aVNQJ7CtVF3SO3miELtJFbBAoMLFLhAcKI6VfmFkytlQywFj2HMHhKcoJIL+NWYNLZUYxea2kQ+fpIEuvYRTE0XqTdQNvDqXcVpsopmCcmZmvETVtXnwRhpS9ok8j+LcntQH/a+p25SWVIlLoQyCV+dJ9CnFh6bAj7gWSYOgDlWY1u2iAvYm+NGZH0nZHkPEXYWXQoyDZvl30w==",
    "key": "XgpUzAkgk4DjLTAxqSPZ7R2mf6NFkdFoFQF1vf+bEsq0CFmAprEtPpwdhKxqi2VDAnIW2xq+411b2LHYrKkPPQ5VAESOPTDGhlMnT/SI6T6IULDwaSXUHw+u9tvfnwzfHRqM5PinQmCrV+NMPRumHKVPNGPcL+bh+5YRWm0E2ZzoPctOKlpXu8B5/8wYQeSFEbIWATL1tazmmcvZrzXKus5OhWU5Orfi6njEM2mw7V4ymlhVdLpxwSbRFQf3WAjqFvrqJl4Gjfn6Ki3ir0Otl8EOe1bQanKX0v+eAOgNlxfE++dXx/x0szgIy/q0yo/dRHbxWaTQAo4PIuC8wDAokQ=="
}
```
- Response:
```
{
    "fullname": "Nguyen Van A",
    "dob": ""
}
```


