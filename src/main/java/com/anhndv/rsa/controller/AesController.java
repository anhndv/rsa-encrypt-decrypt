package com.anhndv.rsa.controller;

import com.anhndv.rsa.model.EncryptDataModel;
import com.anhndv.rsa.model.RequestModel;
import com.anhndv.rsa.util.Common;
import com.google.gson.Gson;
import org.springframework.web.bind.annotation.*;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;

@RestController()
//@RequestMapping("/aes")
public class AesController {

    public static String PUBLIC_KEY_PATH="D:\\key\\ekyc\\rate_publickey_uat.pub";
    public static String PUBLIC_KEY_HSBC_PATH="D:\\key\\ekyc\\hsbc_11793285_provvnm_dev_Encrypt.pub";
    public static String PRIVATE_KEY_PATH="D:\\key\\ekyc\\rate_privatekey_uat.key";
    public static String SIGNING_KEY_PATH="D:\\key\\ekyc\\hsbc_11793285_provvnm_dev_Signing.pkcs8";

    String businessData = "";
    String symmetricKey = "";
    @PostMapping("/encrypt")
    public EncryptDataModel encryptData(HttpServletRequest requestHeader, @RequestBody RequestModel requestBody) throws Exception {
        // get public key and private key from file
        PublicKey publicKey = Common.getPublicKey(PUBLIC_KEY_PATH);
        PrivateKey signingKey = Common.getPrivateKey(SIGNING_KEY_PATH);

        // generate secretKey and encrypt data with AES encrypt
        EncryptDataModel encryptDataModel = new EncryptDataModel();
        SecretKey secretKey = Common.generateKey(256);
        Gson gson = new Gson();
        String secretMessage = gson.toJson(requestBody);
        String data = Base64.getEncoder().encodeToString(Common.aesEncryptCBC(secretKey, secretMessage.getBytes(StandardCharsets.UTF_8)));
        encryptDataModel.setData(data);

        // encrypt secretKey with RSA-2048
        byte[] rawKey = secretKey.getEncoded();
        String keyEncrypt = Common.rsaEncrypt(publicKey, rawKey);
        encryptDataModel.setKey(keyEncrypt);

        //generate signature
        String dataSign = data+keyEncrypt;
        String encodeSign = Common.generateSignature(signingKey, dataSign);
        encryptDataModel.setSign(encodeSign);

        return encryptDataModel;
    }

    @PostMapping("/decrypt")
    public RequestModel decryptData(HttpServletRequest requestHeader, @RequestBody EncryptDataModel requestBody) throws Exception {
        RequestModel requestModel = new RequestModel();
        // get public key and private key from file
        PrivateKey signingKey = Common.getPrivateKey(SIGNING_KEY_PATH);
        PrivateKey privateKey = Common.getPrivateKey(PRIVATE_KEY_PATH);

        // verify signature
        boolean verifySign = verifySignature(signingKey,requestBody);
        if (verifySign) {
            // decrypt to get secretKey
            byte[] keyEncrypt =  Base64.getDecoder().decode(requestBody.getKey());
            byte[] rawKey = Common.rsaDecrypt(privateKey, keyEncrypt);
            SecretKey secretKey = new SecretKeySpec(rawKey, 0, rawKey.length, "AES");;

            // decrypt to get data
            byte[] dataEncrypt = Base64.getDecoder().decode(requestBody.getData().getBytes(StandardCharsets.UTF_8));
            String data = Common.aesDecryptCBC(secretKey, dataEncrypt);
            Gson gson = new Gson();
            requestModel = gson.fromJson(data, RequestModel.class);

            return requestModel;
        } else {
            return null;
        }
    }

    public static boolean verifySignature(PrivateKey privateKey, EncryptDataModel model) {
        try {
            String dataSign = model.getData()+model.getKey();
            String encodeSignature =Common.generateSignature(privateKey,dataSign);
            if (encodeSignature.equals(model.getSign())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

}
