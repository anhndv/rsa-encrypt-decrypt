package com.anhndv.rsa.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RequestModel {
    private String fullname;
    private String dob;
    private String nid1;
    private String nid2;
}
